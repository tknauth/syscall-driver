obj-m+=syscall.o
# https://www.kernel.org/doc/Documentation/kbuild/modules.txt
ccflags-y := -g -ggdb -I/var/tmp/sgx-musl/tools

all: test
	make -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) clean

test : bench.so bench

bench.so : bench.c
	/var/tmp/bin/musl-gcc -o bench.so bench.c -shared -fPIC
