#!/bin/bash

sudo rmmod -f syscall
sudo insmod syscall.ko
DEVICENR=$(grep syscall /proc/devices | cut -f 1 -d' ')
[ -e /dev/syscall0 ] && sudo rm /dev/syscall0
sudo mknod /dev/syscall0 c $DEVICENR 0
sudo chmod a+w /dev/syscall0
