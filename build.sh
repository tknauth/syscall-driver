#/bin/bash

# This sets up the (i) kernel module, (ii) sgx musl, (iii) test
# application.

set -x
set -e

sudo apt-get update
sudo apt-get install -y build-essential git linux-headers-$(uname -r) siege

# sgx-musl
pushd /var/tmp
git clone git@sereca.cloudandheat.com:sereca/sgx-musl.git
pushd sgx-musl
git checkout in_kernel_syscall
CFLAGS="-fPIC" ./configure --prefix=/var/tmp --syslibdir=/var/tmp/lib --disable-shared
make
make install
gcc -o tools/starter tools/starter.c -lpthread -ldl
popd

# kernel driver
git clone https://tknauth@bitbucket.org/tknauth/syscall-driver.git
pushd syscall-driver
make -k
bash setup.sh
popd

# test program
git clone https://tknauth@bitbucket.org/tknauth/mongoose.git
pushd mongoose/examples/restful_server_v2
make CC=/var/tmp/bin/musl-gcc restful_server.so
popd

# to run without the kernel module:
# sudo rmmod syscall

echo now execute the following:
echo /var/tmp/sgx-musl/tools/starter /var/tmp/mongoose/examples/restful_server_v2/restful_server.so

echo create load with:
echo siege -b -c 1 -t 10S http://127.0.0.1:8000/4K
