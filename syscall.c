/**
 * @file    syscall.c
 * @author  
 * @date    
 * @version 
 * @brief  
 */

#include <linux/init.h>             // Macros used to mark up functions e.g., __init __exit
#include <linux/module.h>           // Core header for loading LKMs into the kernel
#include <linux/kernel.h>           // Contains types, macros, functions for the kernel
#include <linux/syscalls.h>
#include <linux/cdev.h>
#include <linux/mm.h>
#include <linux/delay.h>
#include <linux/task_work.h>
#include <linux/vmalloc.h>
#include <asm/msr.h>

#include "starter.h"

int major = 0;
int minor = 0;

/* 1000 seems to be a reasonable number. We have to exit from time to
   time, as certain cleanup actions are only triggered upon exiting
   from the kernel (cf task_work_run()) */
static int syscall_threshold = 1000;
module_param(syscall_threshold, int, 0644);

static bool log = 0;
module_param(log, bool, 0644);

/* Size of the shared memory region (two pages). The first page
   contains enclave_config_t at offset 0. The second page consists of
   syscall_t entries. */
#define BUF_SIZE (2 * 4096)
static char* buf = NULL;

struct my_dev {
  struct cdev cdev;
} my_dev;

typedef long (*sys_call_ptr_t)(long, long, long,
                               long, long, long);

static const char* my_dev_name = "syscall";
static const int nr_devs = 1;
static sys_call_ptr_t *sys_call_table = NULL;

static int syscallsrv_thread(void)
{
  enclave_config_t* ecp = buf;
  volatile syscall_t *S = buf + PAGE_SIZE;
  size_t *readp = &ecp->pointers[READ_P];
  size_t *writep = &ecp->pointers[WRITE_P];
  size_t i;
  unsigned long ret;
  static unsigned long total = 0;
  unsigned long executed_syscalls = 0;

  if (log) printk(KERN_INFO "[>] ioctl()\n");

  do {
    while (*readp >= __atomic_load_n(writep, __ATOMIC_SEQ_CST)) {
        if (ecp->terminate) {
            printk(KERN_INFO "[<] ioctl(), total= %lu, executed= %lu\n", total, executed_syscalls);
            return 0;
        }
    }

    i = *readp % ecp->maxsyscalls;

    ret = sys_call_table[S[i].syscallno](S[i].arg1, S[i].arg2, S[i].arg3,
                                         S[i].arg4, S[i].arg5, S[i].arg6);

    if (log) {
        printk(KERN_INFO "nr=%lu %lu %lu %lu %lu %lu %lu ret=%lu",
               S[i].syscallno,
               S[i].arg1, S[i].arg2, S[i].arg3,
               S[i].arg4, S[i].arg5, S[i].arg6,
               ret);
    }

    total++;
    executed_syscalls++;

    if (ret > -4096UL) {
      S[i].arg6 = -ret;
      S[i].syscallno = -1;
    } else {
      S[i].syscallno = ret;
      S[i].arg6 = 0;
    }
    
    __atomic_add_fetch(readp, 1, __ATOMIC_SEQ_CST); /* prevent write reordering */

  } while(executed_syscalls < syscall_threshold);

  if (log) {
      printk(KERN_INFO "[<] ioctl(), total= %lu\n", total);
  }

  return 0;
}

/* https://bbs.archlinux.org/viewtopic.php?id=139406 */
static sys_call_ptr_t *aquire_sys_call_table(void)
{
	unsigned long int offset = PAGE_OFFSET;
	unsigned long **sct;
	unsigned long i = 0;

	while (offset < ULLONG_MAX) {
		sct = (unsigned long **)offset;

		if (sct[__NR_close] == (unsigned long *) sys_close) {
		  printk(KERN_INFO "%lu iterations to find sys_call_table\n", i);
		  return (sys_call_ptr_t*) sct;
		}

		offset += sizeof(void *);
		++i;
	}
	
	return NULL;
}

// http://stackoverflow.com/questions/10760479/mmap-kernel-buffer-to-user-space/10770582#10770582
static int my_fault(struct vm_area_struct *vma, struct vm_fault *vmf) {
  vmf->page = vmalloc_to_page(buf + (vmf->pgoff << PAGE_SHIFT));
  get_page(vmf->page);
  return 0;
}

static const struct vm_operations_struct mmap_mem_ops = {
  .fault = my_fault
};

int my_mmap(struct file *filp, struct vm_area_struct *vma) {
  int size;

  /* Simple consistency check. If user tries to map more than 2 pages,
     we fail. */
  size = vma->vm_end - vma->vm_start;
  if (size != BUF_SIZE)
    return -EFAULT;

  vma->vm_ops = &mmap_mem_ops;
  /* Not sure if the flags are required. */
  vma->vm_flags |= (VM_DONTEXPAND | VM_DONTDUMP);
  return 0;
}

long my_ioctl(struct file *filp, unsigned int cmd, unsigned long arg) {

  switch (cmd) {
  case IOCTL_SYSCALL: {
      return syscallsrv_thread();

      break;
    }

  default: {
    printk(KERN_WARNING "unknown iotcl cmd %u\n", cmd);
    return 1;
  }
  }

  return 0;
}

struct file_operations fops = {
  .owner = THIS_MODULE,
  .open = nonseekable_open,
  .unlocked_ioctl = my_ioctl,
  .mmap = my_mmap,
  .llseek = no_llseek
};

static void setup_cdev(void)
{
  int err, devno = MKDEV(major, minor);

  cdev_init(&my_dev.cdev, &fops);
  my_dev.cdev.owner = THIS_MODULE;
  my_dev.cdev.ops = &fops;
  err = cdev_add (&my_dev.cdev, devno, 1);
  
  /* Fail gracefully if need be */
  if (err)
    printk(KERN_NOTICE "Error %d adding %s", err, my_dev_name);
}

static int __init syscall_init(void){
   
   int result;
   dev_t dev = 0;

   printk(KERN_INFO "Loaded %s!\n", my_dev_name);
   sys_call_table = aquire_sys_call_table();
   if (sys_call_table == NULL) return 1;

   if (buf == 0) {
       buf = vmalloc_user(BUF_SIZE);
   }

   if (major) {
     dev = MKDEV(major, minor);
     result = register_chrdev_region(dev, nr_devs, my_dev_name);
   } else {
     result = alloc_chrdev_region(&dev, minor, nr_devs,
				  my_dev_name);
     major = MAJOR(dev);
   }
   if (result < 0) {
     printk(KERN_WARNING "%s: can't get major %d\n", my_dev_name, major);
     return result;
   }

   setup_cdev();

   return 0;
}

static void __exit syscall_exit(void){
   dev_t devno = MKDEV(major, minor);

   cdev_del(&my_dev.cdev);
   unregister_chrdev_region(devno, nr_devs);
 
   printk(KERN_INFO "Unloaded %s!\n", my_dev_name);
}

/** @brief A module must use the module_init() module_exit() macros from linux/init.h, which
 *  identify the initialization function at insertion time and the cleanup function (as
 *  listed above)
 */
module_init(syscall_init);
module_exit(syscall_exit);
