#include <sys/time.h>
#include <stdio.h>
#include <sys/syscall.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {

    struct timeval begin, end, diff;
    int repeat = 1000 * 1000 * 1000;
    int i;
    int total = 0;

    if (argc > 1) {
        repeat = strtol(argv[1], NULL, 10);
    }

    gettimeofday(&begin, NULL);
    for (i = 0; i < repeat; ++i) {
        if (i % 1000000 == 0)
            fprintf(stderr, "%d ", i);
        /* total += getpid(); */
        total += syscall(SYS_getpid);
    }
    gettimeofday(&end, NULL);
    printf("\n");
    
    timersub(&end, &begin, &diff);
    printf("%ld.%06ld\n", diff.tv_sec, diff.tv_usec);

    return 0;
}
